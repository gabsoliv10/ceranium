<?php require_once('../components/header.php'); ?>

<main class="page__florianopolis" role="main">
	
	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__florianopolis">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">

					<div class="block__content">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut cupiditate doloremque tempora, corrupti minus qui a earum velit cum officiis placeat et aspernatur voluptatibus, veritatis tempore eaque maxime, aperiam adipisci!
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt deserunt maxime quaerat itaque, dolores dolor, ipsam beatae placeat quis nobis fugit eligendi praesentium reprehenderit. Delectus quae voluptatibus nobis, quidem tempora.
						</p>

					</div>

					<ul class="section__listing">
						<li>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat veritatis sequi omnis libero ipsa eveniet maiores consectetur temporibus hic quo adipisci est, exercitationem laborum laudantium, aliquid molestiae sed! Dolor, sapiente.
						</li>

						<li>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat veritatis sequi omnis libero ipsa eveniet maiores consectetur temporibus hic quo adipisci est, exercitationem laborum laudantium, aliquid molestiae sed! Dolor, sapiente.
						</li>

						<li>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat veritatis sequi omnis libero ipsa eveniet maiores consectetur temporibus hic quo adipisci est, exercitationem laborum laudantium, aliquid molestiae sed! Dolor, sapiente.
						</li>

					</ul>

				</div>

				<div class="col-xs-12 col-sm-12 col-md-6">
					<div class="block__gallery">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<img class="main__picture" src="../assets/images/ceranium/image-01.png" alt="">
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6">
								<img src="../assets/images/florianopolis/image-02.png" alt="">
							</div>

							<div class="col-xs-12 col-sm-6 col-md-6">
								<img src="../assets/images/florianopolis/image-03.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</main>

<?php require_once('../components/footer.php') ?>