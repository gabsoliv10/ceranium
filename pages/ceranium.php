<?php require_once('../components/header.php'); ?>

<main class="page__ceranium" role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__ceranium">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6">

					<div class="block__content">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut cupiditate doloremque tempora, corrupti minus qui a earum velit cum officiis placeat et aspernatur voluptatibus, veritatis tempore eaque maxime, aperiam adipisci!
						</p>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt deserunt maxime quaerat itaque, dolores dolor, ipsam beatae placeat quis nobis fugit eligendi praesentium reprehenderit. Delectus quae voluptatibus nobis, quidem tempora.
						</p>

					</div>

					<ul class="section__listing">
						<li>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat veritatis sequi omnis libero ipsa eveniet maiores consectetur temporibus hic quo adipisci est, exercitationem laborum laudantium, aliquid molestiae sed! Dolor, sapiente.
						</li>

						<li>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat veritatis sequi omnis libero ipsa eveniet maiores consectetur temporibus hic quo adipisci est, exercitationem laborum laudantium, aliquid molestiae sed! Dolor, sapiente.
						</li>

						<li>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat veritatis sequi omnis libero ipsa eveniet maiores consectetur temporibus hic quo adipisci est, exercitationem laborum laudantium, aliquid molestiae sed! Dolor, sapiente.
						</li>

					</ul>

				</div>

				<div class="col-xs-12 col-sm-12 col-md-6">

					<div class="ceranium__history">


						<h2 class="section__title">História</h2>
						
						<div class="block__content">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque recusandae eveniet magnam consectetur illo fuga voluptate voluptatum. Provident adipisci veniam eius odio quo enim at, incidunt esse ut molestias excepturi.
							</p>
						</div>

						<div class="block__gallery">
							<img title="" src="../assets/images/ceranium/image-01.png" alt="">
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="block__team">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">

					<div class="block__winner-team">
						<div class="row">
							<div class="col-md-6">
								<h2>Nosso time é campeão dentro e fora do campo</h2>
							</div>
							<div class="col-md-6">
								<div class="team__image">
									<img src="../assets/images/ceranium/image-time.png" title="Time campeão" alt="Time campeão">
								</div>
							</div>
						</div>
					</div>

					<div class="block__acknowledgments">
						<h2 class="section__title">Reconhecimentos</h2>

						<ul>
							<li>
								<img src="../assets/images/selo-garantia.png" title="Garantia de Entrega" alt="Garantia de Entrega">
							</li>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</section>

	<div class="block__sustentability">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-8">

					<div class="block__content">
						<h2 class="section__title">Sustentabilidade</h2>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem quod eaque fugit quaerat dolorum. Sunt quod, vero voluptates quidem possimus maiores ullam modi atque magnam fuga animi unde, vitae quo distinctio a! Incidunt quam possimus veniam aperiam reprehenderit explicabo quia. Accusamus magnam quae veniam aperiam voluptate, fuga sint libero sapiente quis quisquam ab culpa, voluptatum officiis rem autem repudiandae in.
						</p>

					</div>

				</div>
			</div>
		</div>

	</main>

	<?php require_once('../components/footer.php'); ?>