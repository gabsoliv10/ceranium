<?php require_once('../components/header.php'); ?>

<main role="main" class="enterprise__single">

	<?php include_once('../components/breadcrumb.php'); ?>

	<div class="enterprise__single">

		<section class="enterprise__carousel-wrapper">
			<ul class="owl-carousel owl-theme enterprise__carousel">
				<li>
					<img src="../assets/images/empreendimento-interna/banner-interna.png" title="" alt="">
				</li>

				<li>
					<img src="../assets/images/empreendimento-interna/banner-interna.png" title="" alt="">
				</li>

				<li>
					<img src="../assets/images/empreendimento-interna/banner-interna.png" title="" alt="">
				</li>
			</ul>
		</section>

		<section class="enterprise__info block__section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-9">

						<article class="info__enterprise">
							<div class="row">
								<div class="col-xs-12 col-md-3">
									<div class="info__details">
										<figure class="info__thumbnail">
											<img src="../assets/images/logo-example.png" alt="">
										</figure>

										<div class="info__enterprise-details">
											<h3 class="enterprise__name">Campeche</h3>
											<p class="enterprise__label">Apartamento com 2 ou 3 quartos</p>
										</div>
									</div>	
								</div>

								<div class="col-xs-12 col-md-9">

									<div class="info__description">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt ab sunt adipisci dicta totam esse facere ipsam! Beatae quisquam, eos et, obcaecati reprehenderit qui vero ratione distinctio, perspiciatis omnis minima.
										</p>
									</div>

								</div>
							</div>

							<footer class="info__footer">
								<span class="section__tag tag-forecast">
									Previsão de entrega: <em>Novembro 2018</em>
								</span>

								<span class="info__by">
									<strong>Projeto por: Fulano Beltrano</strong>
								</span>
							</footer>
						</article>

					</div>

					<div class="col-xs-12 col-md-3">

						<aside class="aside__info-contact">
							<h2 class="section__title">Vendas</h2>
							
							<ul class="contact__listing">
								<li>
									<i class="icon icon__tel"></i>
									<a href="tel:+554832228747" title="Entre em Contato">(48) 3222-8747</a>
								</li>
								<li>
									<i class="icon icon__whatsapp"></i>
									<a href="tel:+5548999059999" title="Entre em Contato">(48) 3222-8747</a>
								</li>
							</ul>

							<div class="contact__form-wrapper">

								<p>
									Deixe seu <em>e-mail</em> ou seu <strong>telefone</strong>  que nós entramos em contato com você:
								</p>
								
								<form class="contact__form" action="">
									<fieldset>

										<label for="nome" aria-labelledby="nome">
											<input type="text" name="nome" placeholder="Nome">
										</label>

										<label for="email" aria-labelledby="email">
											<input type="email" name="email" placeholder="email">
										</label>

										<p style="margin-top: 15px; margin-bottom: 15px;">
											Qual o melhor horário para entrarmos em contato?
										</p>

										<label for="horario_contato" aria-labelledby="horario_contato">
											<select name="horario_contato">
												<option value="">Horário</option>
												<option value="1">Opção 1</option>
												<option value="2">Opção 2</option>
											</select>
										</label>

										<input class="btn btn-contact-form" type="submit" value="Enviar">
									</fieldset>
								</form>
							</div>


						</aside>

					</div>
				</div>
			</div>
		</section>

		<section class="enterprise__features block__section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<header>
							<h2 class="section__title">Características</h2>
						</header>
						
						<ul class="section__listing">
							<li>Academia de ginástica</li>
							<li>Acesso à internet</li>
							<li>Acesso para deficientes físicos</li>
							<li>Água quente</li>
							<li>Aquecedor a gás / Água quente</li>
							<li>Ar-condicionado / Split</li>
							<li>Arborização de ruas</li>
							<li>Área verde</li>
							<li>Arvorismo</li>
							<li>Circuito interno de TV / CFTV</li>
							<li>Deck</li>
							<li>Espaço gourmet</li>
							<li>Espaço zen</li>
							<li>Estação tratamento de esgoto</li>
							<li>Estacionamento para visitante</li>
							<li>Garagem privativa</li>
							<li>Gás central</li>
							<li>Gradil</li>
							<li>Jardim</li>
							<li>Muro frontal e/ou lateral</li>
							<li>Pátio</li>
							<li>Pergolado</li>
							<li>Pergolado</li>
							<li>Piscina</li>
							<li>Piscina aquecida</li>
							<li>Playground / Espaço criança</li>
							<li>Port cochère</li>
							<li>Portaria 24h</li>
							<li>Praça</li>
							<li>Reaproveita a água da chuva</li>
							<li>Rede de água / esgoto</li>
							<li>Sala de cinema / Home cinema</li>
							<li>Sala de jogos</li>
							<li>Sala de leitura</li>
							<li>Sala fitiness</li>
							<li>Salão de festas</li>
							<li>Segurança 24h</li>
							<li>Sensores de presença</li>
							<li>Torneiras automáticas</li>
							<li>Trilha / Pista de caminhada</li>
							<li>Válvulas de descarga dual flush</li>
							<li>Vias internas Pavimentadas</li>
							<li>Depósito</li>
						</ul>

					</div>
				</div>
			</div>
		</section>

		<section class="enterprise__images block__carousel block__section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3">

						<div class="content__wrapper">
							<header>
								<h2 class="section__title">Imagens</h2>
							</header>

							<ul class="section__listing">
								<li class="is-active">Área Comum</li>
								<li>Apartamento Tipo 1</li>
								<li>Apartamento Tipo 2</li>
								<li>Cobertura Tipo 1</li>
								<li>Cobertura Tipo 2</li>
							</ul>
						</div>

					</div>

					<div class="col-xs-12 col-sm-12 col-md-9">
						<ul class="owl-carousel owl-theme enterprise__carousel">
							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/carousel-images.png" title="" alt="">
								</a>
							</li>

							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/carousel-images.png" title="" alt="">
								</a>
							</li>

							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/carousel-images.png" title="" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="enterprise__newsletter">
			<div class="container">
				<div class="row">
					<form action="" method="POST">
						<div class="col-sm-6 col-md-3">
							<p class="newsletter__label"><strong>Indique</strong> para um amigo</p>
						</div>

						<div class="col-sm-6 col-md-7">
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<label for="" aria-labelledby="">
										<input type="text" name="" placeholder="Seu Nome">
									</label>
								</div>

								<div class="col-sm-6 col-md-6">
									<label for="" aria-labelledby="">
										<input type="text" name="" placeholder="Seu E-mail">
									</label>
								</div>

								<div class="col-sm-6 col-md-6">
									<label for="" aria-labelledby="">
										<input type="text" name="" placeholder="Seu Nome">
									</label>
								</div>

								<div class="col-sm-6 col-md-6">
									<label for="" aria-labelledby="">
										<input type="text" name="" placeholder="Seu E-mail">
									</label>
								</div>
							</div>
						</div>

						<div class="col-sm-12 col-md-2">
							<input class="btn btn-newsletter" name="" type="submit" value="Enviar">
						</div>
					</form>
				</div>

			</div>
		</section>

		<section class="enterprise__constructions block__carousel block__section no-bg">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-6">

						<header>
							<h2 class="section__title">A obra</h2>
						</header>

						<div class="enterprise__description">
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem magnam libero ut maiores atque perspiciatis deserunt eum. Tenetur quae reprehenderit eligendi possimus, laudantium sed, officia necessitatibus rem dolor voluptatum in!
							</p>

							<div class="enterprise__chart">
								<img src="../assets/images/empreendimento-interna/grafico.png" title="" alt="">
							</div>	
						</div>


					</div>

					<div class="col-xs-12 col-sm-12 col-md-6">
						<ul class="owl-carousel owl-theme enterprise__carousel">
							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/construcao-carousel.png" title="" alt="">
								</a>
							</li>

							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/construcao-carousel.png" title="" alt="">
								</a>
							</li>

							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/construcao-carousel.png" title="" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="enterprise__plans block__carousel block__section">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3">

						<div class="content__wrapper">
							<header>
								<h2 class="section__title">Plantas</h2>
							</header>

							<ul class="section__listing">
								<li class="is-active">Área Comum</li>
								<li>Apartamento Tipo 1</li>
								<li>Apartamento Tipo 2</li>
								<li>Cobertura Tipo 1</li>
								<li>Cobertura Tipo 2</li>
							</ul>
						</div>

					</div>

					<div class="col-xs-12 col-sm-12 col-md-9">
						<ul class="owl-carousel owl-theme enterprise__carousel">
							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/planta-carousel.png" title="" alt="">
								</a>
							</li>

							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/planta-carousel.png" title="" alt="">
								</a>
							</li>

							<li>
								<a href="" title="">
									<img src="../assets/images/empreendimento-interna/planta-carousel.png" title="" alt="">
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="enterprise__newsletter-v2">
			<div class="container">
				<div class="row">
					
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="newsletter__label"><strong>Gostou?</strong> Receba o folder do empreendimento no seu email</span>
					</div>

					<form class="form__download" action="" method="POST">
						<div class="col-xs-12 col-md-12 col-md-9">
							<div class="row">
								<div class="col-sm-6 col-md-4">

									<label for="" aria-labelledby="">
										<input type="text" name="" placeholder="Nome">
									</label>
								</div>

								<div class="col-sm-6 col-md-4">
									<label for="" aria-labelledby="">
										<input type="text" name="" placeholder="E-mail">
									</label>
								</div>

								<div class="col-sm-12 col-md-4">
									<input class="btn btn-download" name="" type="submit" value="Fazer download">
								</div>
							</div>
						</div>
					</form>
				</div>

			</div>
		</section>


		<section class="enterprise__locale block__section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">

						<header>
							<h2 class="section__title">Localização <small>Principais Distâncias</small></h2>
						</header>
						
						<ul class="section__listing">
							<li class="is-active">
								<span class="listing__label">Centro de Florianópolis</span>
								<span class="listing__value">XX Km</span>
							</li>
							<li>
								<span class="listing__label">Aeroporto</span>
								<span class="listing__value">XX Km</span>
							</li>
							<li>
								<span class="listing__label">UFSC</span>
								<span class="listing__value">XX Km</span>
							</li>
							<li>
								<span class="listing__label">UDESC</span>
								<span class="listing__value">XX Km</span>
							</li>
							<li>
								<span class="listing__label">Polícia Federal</span>
								<span class="listing__value">XX Km</span>
							</li>
						</ul>

						<div class="enterprise__map">
							<img src="../assets/images/empreendimento-interna/mapa-interna.png" title="" alt="">
						</div>

					</div>
				</div>
			</div>
		</section>
	</main>

	<?php require_once('../components/footer.php'); ?>