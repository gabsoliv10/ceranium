<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__contact">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					
					<div class="form__block">
						<div class="row">

							<div class="col-xs-12 col-sm-12 col-md-5">
								<form name="contactForm" class="contact__form" method="POST">
									<div class="form__fields">
										<label for="falar_com" aria-labelledby="falar_com">
											<select name="falar_com">
												<option value="">Falar com</option>
												<option value="">Opção 1</option>
												<option value="">Opção 2</option>
												<option value="">Opção 3</option>
											</select>
										</label>

										<label for="nome" aria-labelledby="nome">
											<input type="text" name="nome" placeholder="Nome">
										</label>

										<label for="email" aria-labelledby="email">
											<input type="email" name="email" placeholder="E-mail">
										</label>

										<label for="telefone" aria-labelledby="telefone">
											<input type="tel" name="telefone" placeholder="Telefone">
										</label>

										<label for="mensagem" aria-labelledby="mensagem">
											<textarea name="mensagem" placeholder="Mensagem"></textarea>
										</label>

										<div class="form__actions">
											<input class="btn btn-submit" type="submit" value="Enviar">
										</div>
										
									</div>	
								</form>

								<ul class="form__contact-methods">
									<li>
										<i class="icon icon__tel--blue"></i>
										<a href="+55114832228747" title="">(48) 3222-8747</a>
									</li>
									<li class="pull-right">
										<div class="social__listing">
											<ul>
												<li>
													<a href="" title="Ceranium no Facebook">
														<i class="fa fa-facebook"></i>
													</a>
												</li>
												<li>
													<a href="" title="Ceranium no Instagram">
														<i class="fa fa-instagram"></i>
													</a>
												</li>
												<li>
													<a href="" title="Ceranium no Youtube">
														<i class="fa fa-youtube-play"></i>
													</a>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<i class="icon icon__locale--blue"></i>

										<address>
											R. Felipe Schmidt, 249 - Sala 804
											Centro, Florianópolis - SC, 88010-902
										</address>
									</li>
								</ul>
							</div>


							<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1">
								<div class="block__map">
									<div id="map__wrapper">
										<img src="../assets/images/contato/mapa.png" title="" alt="">
									</div>
								</div>
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</section>

</main>

<div class="contact__footer">
	<?php require_once('../components/footer.php'); ?>

</div>

