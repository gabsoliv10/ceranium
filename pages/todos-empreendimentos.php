<?php require_once('../components/header.php'); ?>

<main role="main">

	<?php include_once('../components/breadcrumb.php'); ?>

	<section class="block__enterprise all__enterprises block__section ">
		<div class="container">
			<div class="listing__all">
				
				<div class="row">
					<div class="col-xs-12">

						<header>
							<h2 class="section__title">Prontos para morar</h2>
						</header>
					</div>
				</div>

				<div class="row">
					<?php for ($i=1; $i <= 12; $i++): ?>
						<div class="col-xs-6 col-sm-4 col-md-3">

							<article class="enterprise__featured">

								<a class="enterprise__image" href="empreendimento_interna.php" title="">
									<img src="../assets/images/empreendimento-small.png" title="" alt="">
								</a>

								<div class="enterprise__wrapper">
									<figure class="enterprise__logo">
										<img src="../assets/images/logo-example.png" title="" alt="">
									</figure>

									<div class="enterprise__description">
										<h3 class="enterprise__name">Campeche</h3>
										<p class="enterprise__label">Apartamento com 2 ou 3 quartos</p>
									</div>
								</div>
							</article>

						</div>
					<?php endfor; ?>
				</div>

			</div>
		</div>
	</section>

</main>

<?php require_once('../components/footer.php'); ?>