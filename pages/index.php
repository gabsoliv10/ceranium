<?php require_once('../components/header.php'); ?>

<main class="page__home" role="main">
	
	<section class="main__banner">
		<ul class="owl-carousel owl-theme main__carousel">
			<li>
				<img src="../assets/images/home/banner-home.png" title="" alt="">
			</li>

			<li>
				<img src="../assets/images/home/banner-home.png" title="" alt="">
			</li>

			<li>
				<img src="../assets/images/home/banner-home.png" title="" alt="">
			</li>
		</ul>
	</section>

	<section class="block__enterprise block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Empreendimentos</h2>
					</header>	

					<div class="enterprises__listing">

						<div class="row">
							<div class="col-xs-12 col-md-12">
								<article class="enterprise__featured main">
									
									<a class="enterprise__image" href="#" title="">
										<img src="../assets/images/empreendimento-destaque.png" title="" alt="">
									</a>

									<div class="enterprise__wrapper">
										<figure class="enterprise__logo">
											<img src="../assets/images/logo-example.png" title="" alt="">
										</figure>

										<div class="section__tag tag-featured">
											<strong>Lançamento</strong>
										</div>

										<div class="enterprise__description">
											<h3 class="enterprise__name">Campeche</h3>
											<p class="enterprise__label">Apartamento com 2 ou 3 quartos</p>
										</div>
									</div>

								</article>
							</div>
						</div>
						<div class="row">
							<?php for ($i=1; $i <= 2; $i++): ?>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<article class="enterprise__featured">

										<a class="enterprise__image" href="empreendimento_interna.php" title="">
											<img src="../assets/images/empreendimento-medium.png" title="" alt="">
										</a>

										<div class="enterprise__wrapper">
											<figure class="enterprise__logo">
												<img src="../assets/images/logo-example.png" title="" alt="">
											</figure>

											<div class="section__tag tag-info">
												<strong>Pronto para morar</strong>
											</div>

											<div class="enterprise__description">
												<h3 class="enterprise__name">Campeche</h3>
												<p class="enterprise__label">Apartamento com 2 ou 3 quartos</p>
											</div>
										</div>
									</article>
								</div>
							<?php endfor; ?>
						</div>						
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="block__blog block__section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<header>
						<h2 class="section__title">Blog</h2>
					</header>	

					<div class="blog__listing">
						<div class="row">
							<?php for ($i=1; $i <= 3; $i++): ?>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<article class="blog__post">
										<div class="post__info" >
											<a class="post__thumbnail" href="blog_interna.php" title=""><img src="../assets/images/home/blog-home.png" title="" alt=""></a>
										</div>

										<div class="post__details">
											<a href="blog_interna.php" title="Leia mais" >
												<h3 class="post__title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
												<div class="post__excerpt">
													<p>
														Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolores, aliquid laboriosam optio dolorem incidunt labore eaque voluptates eum quae ullam deleniti voluptas neque. Consequuntur, minus, quis. Illo, deserunt, facere!
													</p>
												</div>
											</a>

											<div class="section__actions post__actions">
												<a href="blog_interna.php" title="Leia mais" class="btn-read-more">
													<i class="icon icon__angle-left"></i>
													<span class="screen-readers">Continuar lendo</span>
												</a>
											</div>
										</div>
									</article>
								</div>
							<?php endfor; ?>
						</div>						
					</div>

					<div class="section__actions">
						<a class="btn-primary" href="#">Mais posts</a>
					</div>

				</div>
			</div>
		</div>
	</section>

	<?php require_once('../components/newsletter.php') ?>

</main>

<?php require_once('../components/footer.php') ?>