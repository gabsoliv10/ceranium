<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('head.php'); ?>
</head>
<body class="page__home">

  <header class="main__header">
    <div class="container">
      <div class="row align-flex">

        <div class="col-xs-2 col-sm-2 hidden-md hidden-lg ">
          <a href="javascript:void(0)" class="btn-mobile js-mobile-menu hidden-md hidden-lg">
            <span></span>
          </a>

        </div>

        <div class="col-xs-8 col-sm-8 col-md-2 text-center">
          <figure class="header__logo">
            <a href="" title="">
              <img src="../assets/images/logo.png" title="Página Inicial" alt="Página Inicial">
            </a>
          </figure>
        </div>   

        <div class="col-xs-12 col-md-8 col-md-offset-4 hidden-sm">
          <ul class="social__listing pull-right hidden-xs hidden-sm">
            <li>
              <i class="icon icon__tel--blue--small"></i><a href="tel:+554832228747">(48) 3222-8747</a>
            </li>
            <li>
              <a href="" title="Ceranium no Facebook">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="" title="Ceranium no Instagram">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
            <li>
              <a href="" title="Ceranium no Youtube">
                <i class="fa fa-youtube-play"></i>
              </a>
            </li>
          </ul>
          <ul class="header__nav">
           <li><a href="empreendimentos.php" title="Empreendimentos">Empreendimentos</a></li>
           <li><a href="florianopolis.php" title="Florianópolis">Florianópolis</a></li>
           <li><a href="ceranium.php" title="Ceranium">Ceranium</a></li>
           <li><a href="contato.php" title="Fale Conosco">Fale Conosco</a></li>
         </ul>
       </div>    

     </div>
   </div>
 </header>