<footer class="main__footer">
	<div class="container">
		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-5">

				<div class="block__about">
					<a class="footer__logo" href="" title="">
						<img src="../assets/images/logo-negative.png" title="Construtora Ceranium" alt="Construtora Ceranium">
					</a>

					<div class="footer__description">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa repellat doloremque excepturi ut totam sint illum.
						</p>
					</div>
				</div>

				<div class="footer__block-1">
					<div class="block__social">

						<ul class="contact__listing">
							<li>
								<i class="icon icon__tel"></i>
								<a href="tel+554832228747" title="Entre em Contato">(48) 3222-8747</a>
							</li>
							<li>
								<i class="icon icon__locale"></i>
								<address>
									R. Felipe Schmidt, 249 - Sala 804
									Centro, Florianópolis - SC, 88010-902
								</address>
							</li>
						</ul>
						<ul class="social__listing pull-right">
							<li>
								<a href="" title="Ceranium no Facebook">
									<i class="fa fa-facebook"></i>
								</a>
							</li>
							<li>
								<a href="" title="Ceranium no Instagran">
									<i class="fa fa-instagram"></i>
								</a>
							</li>
							<li>
								<a href="" title="Ceranium no Youtube">
									<i class="fa fa-youtube-play"></i>
								</a>
							</li>
						</ul>
					</div>
					
				</div>				

			</div>
			
			<div class="col-xs-12 col-sm-12 col-md-2 trip-advisor">
				<div class="footer__block-2">
					<a href="" title="">
						<i class="icon icon__shipping-guarantee"></i>
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-3 col-md-offset-2 pull-right">
				<div class="footer__block2 pull-right">
					<img src="../assets/images/facebook.png" title="" alt="">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<figure class="code__logo pull-right">
					<a href="http://codde.com.br/" title="Desenvolvido por Code"><img src="../assets/images/logo-codde.png" alt=""></a>
				</figure>
			</div>
		</div>
	</div>
</footer>

<section class="footer__contact">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				
				<div class="contact__wrapper">
					<p>
						Entre em contato, <a href="mailto:contato@ceranium.com.br" title="Envie um e-mail para a Ceranium">contato@ceranium.com.br</a> que ligamos para você
					</p>

					<ul class="contact__formlist pull-right">
						<li>
							<span class="label__name">Telefone</span>
							<a href="tel:+55483475555557">(48) 34755 - 55557</a>
						</li>

						<li>
							<span class="label__name">Whatsapp</span>
							<a href="tel:+5548999999999">(48) 99999-9999</a>
						</li>
					</ul>
				</div>

			</div>
		</div>
	</div>
</section>

<script async defer src="https://use.fontawesome.com/4d634eeb4a.js"></script>

<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="../assets/js/owl.carousel.min.js"></script>
<script src="../assets/js/app.js"></script>

</body>
</html>